package com.app.byte_start.view

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.app.byte_start.data_base.model.UserEntity
import com.app.byte_start.databinding.ActivityMainBinding
import com.app.byte_start.view.home.HomeActivity
import com.app.byte_start.view.register.RegisterActivity
import com.app.byte_start.view_models.CommonViewModel
import com.app.byte_start.view_models.UserFetchListener
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity(),UserFetchListener {
    private lateinit var binding: ActivityMainBinding
    private val sharedViewModel: CommonViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnStart.setOnClickListener {

            val username = binding.etLoginUsername.text.toString()
            if(username.isNotEmpty()){
                sharedViewModel.fetchUsersByUsername(username)
            }

        }
        sharedViewModel.setUserFetchListener(this)
        binding.tvRegister.setOnClickListener{
            val intent = Intent(this@MainActivity, RegisterActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onUserFetchListener(users: List<UserEntity>) {
        if(users.isNotEmpty()){
            val username = binding.etLoginUsername.text.toString()
            val password = binding.etLoginPassword.text.toString()

            val dbUsername = users[0].username
            val dbPassword = users[0].password

            if(username == dbUsername && password == dbPassword){
                val intent = Intent(this@MainActivity, HomeActivity::class.java)
                startActivity(intent)
            }
        }

    }

}
