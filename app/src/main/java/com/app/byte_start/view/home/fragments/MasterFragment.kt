package com.app.byte_start.view.home.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.byte_start.R
import com.app.byte_start.databinding.FragmentMasterBinding


class MasterFragment : Fragment() {

    private lateinit var binding: FragmentMasterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMasterBinding.inflate(layoutInflater)
        return binding.root
    }

}