package com.app.byte_start.view.register

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.app.byte_start.data_base.dao.UserDao
import com.app.byte_start.data_base.model.UserEntity
import com.app.byte_start.databinding.ActivityMainBinding
import com.app.byte_start.databinding.ActivityRegisterBinding
import com.app.byte_start.view.home.HomeActivity
import com.app.byte_start.view_models.CommonViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class RegisterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRegisterBinding

    private val sharedViewModel: CommonViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRegister.setOnClickListener {
            val password = binding.etPassword.text.toString()
            val confirmPassword = binding.etConfirmPassword.text.toString()
            val username = binding.etUsername.text.toString()
            if(username.isNotEmpty() && password.isNotEmpty() && (password == confirmPassword)){
                try {
                    sharedViewModel.insertUser(UserEntity(username=username, password = password))
                    finish()
                }catch (exception: Exception){
                    exception.printStackTrace()
                }

            }

        }

    }
}