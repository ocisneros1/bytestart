package com.app.byte_start.data_base.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_entity")
class UserEntity(@PrimaryKey(autoGenerate = true)
                     var id: Integer? = null,var username: String, var password: String) {
}