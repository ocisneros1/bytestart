package com.app.byte_start.data_base.repositories

import com.app.byte_start.data_base.dao.UserDao
import com.app.byte_start.data_base.model.UserEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UserRepository @Inject constructor(private val userDao: UserDao) {
    fun getAllData(): Flow<List<UserEntity>> {
        return userDao.getUserListFlow()
    }

    fun getDataByUsername(username: String): List<UserEntity> {
        return userDao.getUserByUsername(username)
    }

    suspend fun insertRegister(user: UserEntity){
        userDao.insert(user)
    }
}