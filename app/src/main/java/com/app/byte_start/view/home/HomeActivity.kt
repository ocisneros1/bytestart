package com.app.byte_start.view.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.app.byte_start.databinding.ActivityHomeBinding
import com.app.byte_start.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

}
