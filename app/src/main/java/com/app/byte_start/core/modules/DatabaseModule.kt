package com.app.byte_start.core.modules

import android.app.Application
import androidx.room.Room
import com.app.byte_start.data_base.ByteDatabase
import com.app.byte_start.data_base.dao.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Singleton
    @Provides
    fun provideRoomDatabase(application: Application?): ByteDatabase {
        return Room.databaseBuilder(application!!, ByteDatabase::class.java, "data_log_db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideDataLogDao(database: ByteDatabase): UserDao {
        return database.userDao()
    }
}