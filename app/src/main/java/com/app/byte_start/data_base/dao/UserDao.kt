package com.app.byte_start.data_base.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.app.byte_start.data_base.model.UserEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface UserDao {

    @Query("SELECT * FROM user_entity ORDER BY id DESC")
    fun getUserList(): List<UserEntity>

    @Query("SELECT * FROM user_entity where username = :username  ORDER BY id DESC")
    fun getUserByUsername(username: String): List<UserEntity>
    @Query("SELECT * FROM user_entity ORDER BY id DESC")
    fun getUserListFlow(): Flow<List<UserEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(user: UserEntity)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertList(users: List<UserEntity>)

    @Query("DELETE FROM user_entity")
    fun deleteAllUser()

}