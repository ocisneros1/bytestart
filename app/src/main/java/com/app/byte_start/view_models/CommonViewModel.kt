package com.app.byte_start.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.byte_start.data_base.model.UserEntity
import com.app.byte_start.data_base.repositories.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


interface UserFetchListener {
    fun onUserFetchListener(users: List<UserEntity>)
}
@HiltViewModel
class CommonViewModel @Inject constructor(private val userRepository: UserRepository) : ViewModel() {


    private var userFetchListener: UserFetchListener? = null

    fun setUserFetchListener(listener: UserFetchListener) {
        userFetchListener = listener
    }

    fun fetchUsersByUsername(username: String) {
        viewModelScope.launch {
            val users = withContext(Dispatchers.IO) {
                userRepository.getDataByUsername(username)
            }
            userFetchListener?.onUserFetchListener(users)
        }
    }
    fun insertUser(user: UserEntity){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                userRepository.insertRegister(user)
            }
        }
    }


}