package com.app.byte_start.data_base

import androidx.room.Database
import androidx.room.RoomDatabase
import com.app.byte_start.data_base.dao.UserDao
import com.app.byte_start.data_base.model.UserEntity


@Database(entities = [UserEntity::class], version = 1, exportSchema = false)
abstract class  ByteDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

}